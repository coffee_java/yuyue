<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <#include "/common/css.ftl"/>
    <title>主机列表</title>
    <script language="javascript">
    	function showActionSheet(yuyueId){
    		$.actions({
	    	  title: "选择操作",
	          actions: [
			  {
			    className: "bg-danger",
			    text: "删除",
			    onClick: function() {
			    $.confirm({
				  title: '请确认',
				  text: '是否确认删除',
				  onOK: function () {
				      //点击确认
				      $.ajax({
						  url:'/yuyue/delete/'+yuyueId,
						  type:'post',
						  data:{'${_csrf.parameterName}':'${_csrf.token}'},
						  dataType:'json',
						  success:function(result){
							if (result) {
						      $.toast("操作成功");
						      window.location.reload();
							}else{
						        $.toast("操作失败", "forbidden");
							}
							  
						  }
					  	});
				  }
				});
			    }
			  }]
			});
    	}
    </script>
    </head>
    <#assign urlpath="yuyueList">
    <body>
	    <#include "/common/title.ftl"/>
	     <#if page.content?? && (page.content?size > 0) >
		    <div class="bd">
	    		<div class="weui_cells">
				    <#list page.content as yuyue>
				    	<div class="weui_cell" onclick="showActionSheet('${yuyue.id}')">
				          <div class="weui_cell_bd weui_cell_primary">
				            <p>预约时间：${yuyue.day}<br/>预约产品：${yuyue.product.content}</p>
				          </div>
				          <div class="weui_cell_ft"><font class="red">预约人：${yuyue.userName}</font><br/><font class="red">联系电话：${yuyue.phone}</font></div>
				        </div>
					</#list>
		    	</div>
			</div>
 		<#else>
 		<div class="weui_msg">
		  <div class="weui_text_area">
		    <h2 class="weui_msg_title">暂时没有预约</h2>
		  </div>
		</div>
 		</#if>
    <#include "/common/tabar.ftl"/>
    </body>
    <#include "/common/js.ftl"/>
</html>