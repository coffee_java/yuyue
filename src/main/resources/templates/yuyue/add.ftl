<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <#include "/common/css.ftl"/>
    <title>预约${user.userName}服务</title>
    <script language="javascript">
    	function changeDate(obj,userId){
    		//点击确认
		      $.ajax({
				  url:'/yuyue/add/getLimit/'+userId,
				  type:'post',
				  data:{'${_csrf.parameterName}':'${_csrf.token}','date':$(obj).val()},
				  dataType:'text',
				  success:function(result){
				      $.toast("所选日期剩余人数："+result);
				      $("#limit").html(result);
				      $("#limitContent").show();
				  }
			  	});
    	}
    	function yuyue(productId){
    		var date=$("#yuyueshijian").val();
    		var userName=$("#userName").val();
    		var phone=$("#phone").val();
    		if(userName==""){
    			$.toptip('请填写预约人', 'error');
    			return;
    		}else if(phone==""){
    			$.toptip('请填写联系电话', 'error');
    			return;
    		}else if(date==""){
    			$.toptip('请选择预约时间', 'error');
    			return;
    		}
    		$.showLoading();
    		$.ajax({
				  url:'/yuyue/add',
				  type:'post',
				  data:{
					  '${_csrf.parameterName}':'${_csrf.token}',
					  'productId':productId,
					  'day':date,
					  'userName':userName,
					  'phone':phone
					  },
				  dataType:'json',
				  success:function(result){
				      $.hideLoading();
					  if (result.result) {
					      $.toast("预约成功");
						}else{
					        $.toast("预约失败："+result.message, "forbidden");
						}
				  }
			  	});
    	}
    </script>
    </head>
    <#assign urlpath="productList">
    <body>
	    <header class="wol-header">
		  <h1 class="wol-title">预约${user.userName}服务</h1>
		  <p class="wol-sub-title">联系电话：${user.phone}</p> 
		  <p class="wol-sub-title">地址：${user.address}</p> 
		</header>
	     <#if page.content?? && (page.content?size > 0) >
		    <div class="bd">
	    		<div class="weui_cells">
	    			<div class="weui_cell">
					    <div class="weui_cell_hd"><label for="" class="weui_label">预约人姓名</label></div>
					    <div class="weui_cell_bd weui_cell_primary">
					      <input class="weui_input" type="text" name="userName" value="" id="userName" placeholder="请输入预约人姓名">
					    </div>
					</div>
	    			<div class="weui_cell">
					    <div class="weui_cell_hd"><label for="" class="weui_label">联系电话</label></div>
					    <div class="weui_cell_bd weui_cell_primary">
					      <input class="weui_input" type="text" name="phone"  value="" id="phone" placeholder="请输入联系电话">
					    </div>
					</div>
	    			<div class="weui_cell">
					    <div class="weui_cell_hd"><label for="" class="weui_label">预约日期</label></div>
					    <div class="weui_cell_bd weui_cell_primary">
					      <input class="weui_input" type="date" value="" id="yuyueshijian" onchange="changeDate(this,${user.id})">
					    </div>
					</div>
					<div class="weui_cells_title red"><font id="limitContent" style="display: none">（所选日期剩余名额：<font id="limit"></font>）</font></div>
					<div class="weui_cells_title red">填写好以上预约信息后再点击产品进行预约</div>
				    <#list page.content as product>
				    	<div class="weui_cell" onclick="yuyue('${product.id}')">
				          <div class="weui_cell_bd weui_cell_primary">
				            <p>${product.content}</p>
				          </div>
				          <div class="weui_cell_ft"><font class="huaxian">原价：${product.yuanjia}</font>&nbsp;&nbsp;<font class="red">现价：${product.xianjia}</font></div>
				        </div>
					</#list>
		    	</div>
			</div>
 		<#else>
 		<div class="weui_msg">
		  <div class="weui_text_area">
		    <h2 class="weui_msg_title">暂时没有产品</h2>
		  </div>
		</div>
 		</#if>
    </body>
    <#include "/common/js.ftl"/>
</html>