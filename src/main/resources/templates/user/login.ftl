<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <#include "/common/css.ftl"/>
    <title>登录页面</title>
    </head>
    <body>
	    <#include "/common/title.ftl"/>
        <form action="/user/login" method="POST">
	    <div class="weui_cells weui_cells_form">
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">用户</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input class="weui_input" type="text" name="username" placeholder="请输入用户名">
		    </div>
		  </div>
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">密码</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input class="weui_input" type="password" name="password" placeholder="请输入密码">
		    </div>
		  </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</div>
		<br/>
        <div>
        <input class="weui_btn weui_btn_primary" type="submit" name="submit" value="登录"/>
        </div>
        </form>
    </body>
    <#include "/common/js.ftl"/>
</html>