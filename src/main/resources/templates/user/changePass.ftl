<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <#include "/common/css.ftl"/>
    <title>修改密码</title>
    </head>
    <#assign urlpath="userProfile">
    <body>
	    <#include "/common/title.ftl"/>
        <form id="form" action="/user/changePass" method="POST">
	    <div class="weui_cells weui_cells_form">
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">原密码</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input class="weui_input" type="password" name="oldPassword" placeholder="请输入原密码">
		    </div>
		  </div>
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">新密码</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input id="password" class="weui_input" type="password" name="password" placeholder="请输入新密码">
		    </div>
		  </div>
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">确认密码</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input id="passwordComfirm" class="weui_input" type="password"  placeholder="请再次输入密码">
		    </div>
		  </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</div>
		<br/>
        <div>
        <input class="weui_btn weui_btn_primary" type="submit" name="submit" value="确定"/>
        </div>
        </form>
        <#include "/common/tabar.ftl"/>
    </body>
    <#include "/common/js.ftl"/>
</html>