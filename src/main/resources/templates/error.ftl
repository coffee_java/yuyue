<!DOCTYPE html>  
  
<html lang="zh_CN">  
<head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
 </head>
 
<body>  
    Something wrong: ${status} ${error}  
</body>  
  
</html>  