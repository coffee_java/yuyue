<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
<meta name="x5-page-mode" content="app">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <#include "/common/css.ftl"/>
    <title>主机列表</title>
    </head>
    <#assign urlpath="productList">
    <body>
	<#include "/common/title.ftl"/>
	   <form action="/product/add" method="POST">
	   <div class="weui_cells weui_cells_form">
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">产品名称</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input class="weui_input" type="text" name="content" placeholder="请输入产品名称">
		    </div>
		  </div>
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">原价</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input class="weui_input" type="number" name="yuanjia" placeholder="请输入原价">
		    </div>
		  </div>
		  <div class="weui_cell">
		    <div class="weui_cell_hd"><label class="weui_label">现价</label></div>
		    <div class="weui_cell_bd weui_cell_primary">
		      <input class="weui_input" type="number" name="xianjia" placeholder="请输入现价">
		    </div>
		  </div>
		</div>
		<br/>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		 <input class="weui_btn weui_btn_primary" type="submit" name="submit" value="添加"/>
		</form>
    <#include "/common/tabar.ftl"/>
    </body>
    <#include "/common/js.ftl"/>
</html>