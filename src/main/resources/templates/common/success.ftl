<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<!-- UC强制全屏 -->
	<meta name="full-screen" content="yes">
	<!-- QQ强制全屏 -->
	<meta name="x5-fullscreen" content="true">
	<!-- UC应用模式 -->
	<meta name="browsermode" content="application">
	<!-- QQ应用模式 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <#include "/common/css.ftl"/>
    <title>操作成功</title>
    </head>
    <body>
	<#include "/common/title.ftl"/>
	   <div class="weui_msg">
		  <div class="weui_icon_area"><i class="weui_icon_success weui_icon_msg"></i></div>
		  <div class="weui_text_area">
		    <h2 class="weui_msg_title">操作成功</h2>
		  </div>
		  <div class="weui_opr_area">
		    <p class="weui_btn_area">
		      <a href="/" class="weui_btn weui_btn_primary">确定</a>
		    </p>
		  </div>
		</div>
    <#include "/common/tabar.ftl"/>
    </body>
    <#include "/common/js.ftl"/>
</html>