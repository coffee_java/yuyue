<!-- body 最后 -->
<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/0.8.3/js/jquery-weui.min.js"></script>
<script language="javascript">
    	function showUserActionSheet(){
    		$.actions({
	    	  title: "选择操作",
	          actions: [{
	          	className: "bg-danger",
			    text: "生成推广链接",
			    onClick: function() {
			    	window.location.href="/user/tuiguang"
			    }
			  },
			  {
	          	className: "bg-success",
			    text: "修改密码",
			    onClick: function() {
			    	window.location.href="/user/changePass"
			    }
			  },
			  {
	          	className: "bg-warning",
			    text: "修改用户信息",
			    onClick: function() {
			    	window.location.href="/user/changeProfile"
			    }
			  },
			  {
			    className: "bg-danger",
			    text: "退出",
			    onClick: function() {
			    	$("#logoutForm").submit();
			    }
			  }]
			});
    	}
    	function showProductActionSheet(){
    		$.actions({
	    	  title: "选择操作",
	          actions: [{
	          	className: "bg-primary",
			    text: "产品列表",
			    onClick: function() {
			    	window.location.href="/product/list"
			    }
			  },
			  {
			    className: "bg-warning",
			    text: "添加产品",
			    onClick: function() {
			    	window.location.href="/product/add"
			    }
			  }]
			});
    	}
</script>
<form id="logoutForm" action="/logout" method="POST">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</from>