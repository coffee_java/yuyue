<div class="weui_tabbar">
        <a href="javascript:showProductActionSheet();" class="weui_tabbar_item <#if urlpath??&&urlpath == "productList">weui_bar_item_on</#if>">
          <div class="weui_tabbar_icon">
            <img src="/images/icon_nav_button.png" alt="">
          </div>
          <p class="weui_tabbar_label">产品列表</p>
        </a>
        <a href="/yuyue/list" class="weui_tabbar_item <#if urlpath??&& urlpath == "yuyueList">weui_bar_item_on</#if>">
          <div class="weui_tabbar_icon">
            <img src="/images/icon_nav_button.png" alt="">
          </div>
          <p class="weui_tabbar_label">预约列表</p>
        </a>
        <a href="javascript:showUserActionSheet();" class="weui_tabbar_item <#if urlpath??&& urlpath == "userProfile">weui_bar_item_on</#if>">
          <div class="weui_tabbar_icon">
            <img src="/images/icon_nav_cell.png" alt="">
          </div>
          <p class="weui_tabbar_label">个人中心</p>
        </a>
      </div>