package com.coffee.yuyue.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coffee.yuyue.dao.UserDao;
import com.coffee.yuyue.entity.User;

@Service
@Transactional(readOnly=false)
public class UserService {
	@Autowired
	private UserDao userDao;
	
	public User get(Long id){
		return userDao.findOne(id);
	}
	public void save(User user){
		userDao.save(user);
	}
	public User findByLoginName(String loginName){
		return userDao.findByLoginName(loginName);
	}
}
