package com.coffee.yuyue.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coffee.yuyue.dao.YuyueDao;
import com.coffee.yuyue.entity.Yuyue;

@Service
@Transactional(readOnly=false)
public class YuyueService {
	@Autowired
	private YuyueDao yuyueDao;
	
	public Yuyue get(Long id){
		return yuyueDao.findOne(id);
	}
	public void add(Yuyue yuyue){
		yuyueDao.save(yuyue);
	}
	public void delete(Yuyue yuyue){
		yuyue.setIsDel(true); 
		yuyueDao.save(yuyue);
	}
	public Page<Yuyue> findByUserId(Long userId,PageRequest pageRequest){
		return yuyueDao.findByUserIdAndIsDel(userId,false,pageRequest);
	}
	public List<Yuyue>  findByUserIdAndDayAndIsDel(Long userId,String day){
		return yuyueDao.findByUserIdAndDayAndIsDel(userId, day, false);
	}
}
