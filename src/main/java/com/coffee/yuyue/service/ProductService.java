package com.coffee.yuyue.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coffee.yuyue.dao.ProductDao;
import com.coffee.yuyue.entity.Product;

@Service
@Transactional(readOnly=false)
public class ProductService {
	@Autowired
	private ProductDao productDao;
	
	public Product get(Long id){
		return productDao.findOne(id);
	}
	public void add(Product product){
		productDao.save(product);
	}
	public void delete(Product product){
		product.setIsDel(true); 
		productDao.save(product);
	}
	public Page<Product> findByUserId(Long userId,PageRequest pageRequest){
		return productDao.findByUserIdAndIsDel(userId,false,pageRequest);
	}
}
