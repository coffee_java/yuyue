package com.coffee.yuyue.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
//@Table(uniqueConstraints={@UniqueConstraint(columnNames={"mac"})})
@Table
public class Yuyue implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1895880152720102132L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	@NotNull
	private Long userId;
	
	@Column(nullable = false)
	@NotBlank
	private String day;
	@Column(nullable = false)
	@NotBlank
	private String userName;
	
	@Column(nullable = false)
	@NotBlank
	private String phone;
	
	@Column(insertable=false,updatable=false)
	private Long productId;
	
	@ManyToOne
	@JoinColumn(name = "productId")
	private Product product;
	
	@NotNull
	private Boolean isDel;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Boolean getIsDel() {
		return isDel;
	}
	public void setIsDel(Boolean isDel) {
		this.isDel = isDel;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
}
