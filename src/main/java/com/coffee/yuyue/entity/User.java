package com.coffee.yuyue.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"loginName"})})
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7199506755931322827L;

	
	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	@NotBlank
	private String loginName;

	@Column(nullable = false)
	@NotBlank
	@JsonIgnore
	private String password;

	@Column(nullable = false)
	@NotBlank
	private String userName;
	@Column(nullable = false)
	@NotBlank
	private String phone;
	@Column(nullable = false)
	@NotBlank
	private String address;
	/**
	 * 每天限额人数
	 */
	@Column(nullable = false)
	@NotNull
	private Integer limitDay;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getLimitDay() {
		return limitDay;
	}

	public void setLimitDay(Integer limitDay) {
		this.limitDay = limitDay;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
