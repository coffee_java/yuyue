package com.coffee.yuyue.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
//@Table(uniqueConstraints={@UniqueConstraint(columnNames={"mac"})})
@Table
public class Product implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1895880152720103416L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	@NotNull
	private Long userId;
	
	@Column(nullable = false)
	@NotBlank
	private String content;
	
	@NotNull
	private Double yuanjia;
	
	@NotNull
	private Double xianjia;
	
	@NotNull
	private Boolean isDel;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Double getYuanjia() {
		return yuanjia;
	}
	public void setYuanjia(Double yuanjia) {
		this.yuanjia = yuanjia;
	}
	public Double getXianjia() {
		return xianjia;
	}
	public void setXianjia(Double xianjia) {
		this.xianjia = xianjia;
	}
	public Boolean getIsDel() {
		return isDel;
	}
	public void setIsDel(Boolean isDel) {
		this.isDel = isDel;
	}
}
