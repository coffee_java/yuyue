package com.coffee.yuyue.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Component
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	 @Autowired
	 private MyAuthenticationProvider provider;//自定义验证
	 @Autowired
     public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception{
     }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.authorizeRequests().antMatchers("/css/*","/js/*","/images/*","/yuyue/add","/yuyue/add/*","/yuyue/add/*/*").permitAll()  
    	.anyRequest().authenticated()  
    	.and().formLogin()
    	.loginProcessingUrl("/user/login").permitAll()
    	.loginPage("/user/login")
    	.and().logout().permitAll()
    	.logoutUrl("/logout").logoutSuccessUrl("/user/login");  
    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        //将验证过程交给自定义验证工具
        auth.authenticationProvider(provider);
    }
}