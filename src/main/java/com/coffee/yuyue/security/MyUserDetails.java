package com.coffee.yuyue.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.coffee.yuyue.entity.User;

public class MyUserDetails extends User implements UserDetails{

    /**
	 * 
	 */
	private static final long serialVersionUID = -6121110141725709430L;

	
	
	
	public MyUserDetails() {
		super();
	}
	public MyUserDetails(User user) {
		super.setId(user.getId());
		super.setLoginName(user.getLoginName());
		super.setPassword(user.getPassword());
		super.setUserName(user.getUserName());
	}

	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
         return AuthorityUtils.commaSeparatedStringToAuthorityList("");
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}