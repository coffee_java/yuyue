package com.coffee.yuyue.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.coffee.yuyue.entity.Yuyue;

public interface  YuyueDao extends PagingAndSortingRepository<Yuyue, Long>{
	Page<Yuyue> findByUserIdAndIsDel(Long userId,Boolean isDel,Pageable pageRequest);
	List<Yuyue> findByUserIdAndDayAndIsDel(Long userId,String day,Boolean isDel);
}
