package com.coffee.yuyue.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.coffee.yuyue.entity.Product;

public interface  ProductDao extends PagingAndSortingRepository<Product, Long>{
	Page<Product> findByUserIdAndIsDel(Long userId,Boolean isDel,Pageable pageRequest);
}
