package com.coffee.yuyue.dao;

import org.springframework.data.repository.CrudRepository;

import com.coffee.yuyue.entity.User;

public interface  UserDao extends CrudRepository<User, Long>{
	User findByLoginName(String loginName);
}
