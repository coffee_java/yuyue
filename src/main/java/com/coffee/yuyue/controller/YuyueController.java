package com.coffee.yuyue.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.coffee.yuyue.entity.Product;
import com.coffee.yuyue.entity.User;
import com.coffee.yuyue.entity.Yuyue;
import com.coffee.yuyue.security.MyUserDetails;
import com.coffee.yuyue.service.ProductService;
import com.coffee.yuyue.service.UserService;
import com.coffee.yuyue.service.YuyueService;

@Controller
@RequestMapping(value="/yuyue")
public class YuyueController {
	@Autowired
	private YuyueService yuyueService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/list",method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(required=true,defaultValue="0")Integer pageNo) {
		MyUserDetails userDetails = (MyUserDetails) SecurityContextHolder.getContext()
			    .getAuthentication()
			    .getPrincipal();
		PageRequest pageRequest=new PageRequest(pageNo, 20);
		Page<Yuyue> page=yuyueService.findByUserId(userDetails.getId(),pageRequest);
		ModelAndView mv=new ModelAndView("yuyue/list");
		mv.addObject("page",page);
        return mv;
    }
	@RequestMapping(value="/delete/{yuyueId}",method = RequestMethod.POST)
	@ResponseBody
	public boolean delete(@PathVariable Long yuyueId) {
		MyUserDetails userDetails = (MyUserDetails) SecurityContextHolder.getContext()
				.getAuthentication()
				.getPrincipal();
		Yuyue yuyue=yuyueService.get(yuyueId);
		if (yuyue!=null&&yuyue.getUserId()==userDetails.getId()) {
			yuyueService.delete(yuyue);
			return true;
		}
		return false;
	}
	
	//以下功能为客户使用
	
	@RequestMapping(value="/add/{userId}",method = RequestMethod.GET)
	public ModelAndView add(@PathVariable Long userId) {
		PageRequest pageRequest=new PageRequest(0, 2000);
		User user=userService.get(userId);
		Page<Product> page=productService.findByUserId(userId,pageRequest);
		ModelAndView mv=new ModelAndView("yuyue/add");
		mv.addObject("user",user);
		mv.addObject("page",page);
        return mv;
	}
	@ResponseBody
	@RequestMapping(value="/add/getLimit/{userId}",method = RequestMethod.POST)
	public Integer getLimit(@PathVariable Long userId,String date) {
		User user=userService.get(userId);
		List<Yuyue> result=yuyueService.findByUserIdAndDayAndIsDel(userId, date);
		int have=result==null?0:result.size();
		int limit=user.getLimitDay();
		int shengyu=limit-have;
		return shengyu<0?0:shengyu;
	}
	@ResponseBody
	@RequestMapping(value="/add",method = RequestMethod.POST)
	public Map<String, Object> addFrom(Yuyue yuyue) {
		Product product=productService.get(yuyue.getProductId());
		
		Map<String, Object> map=new HashMap<String, Object>();
		User user=userService.get(product.getUserId());
		List<Yuyue> result=yuyueService.findByUserIdAndDayAndIsDel(product.getUserId(), yuyue.getDay());
		int have=result==null?0:result.size();
		int limit=user.getLimitDay();
		int shengyu=limit-have;
		if (shengyu<=0) {
			map.put("result", false);
			map.put("message", "今日已约满，请约其他日期");
			return map;
		}
		yuyue.setUserId(product.getUserId());
		yuyue.setProduct(product);
		yuyue.setIsDel(false);
		yuyueService.add(yuyue);
//		ModelAndView mv=new ModelAndView("common/success");
//		mv.addObject("redirectUrl","/yuyue/add/"+product.getUserId());
		map.put("result", true);
		return map;
	}
}
